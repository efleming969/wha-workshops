var S = require( "sequelize" )

var sequelize = new S( "", "", ""
, { dialect: "sqlite"
  , storage: "data.sqlite"
  } )

var accounts = exports.accounts = sequelize.define( "accounts"
, { name: S.STRING
  , age: S.BIGINT
  } )

exports.initialize = function( callback ) {
  accounts.sync().then( callback )
}
