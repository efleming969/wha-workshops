var Hapi = require( "hapi" )
var Models = require( "./Models" )

var server = new Hapi.Server()

server.register( [ require( "vision" ) ], function( err ) {
  server.views( {
    engines: { html: require( "handlebars" ) },
    relativeTo: __dirname,
    path: "templates"
  } )
} )

server.connection( {
  host: "localhost",
  port: 3001,
  routes: { cors: true },
} )

server.route( {
  method: "GET",
  path: "/",
  handler: function( request, reply ) {
    reply.view( "index", { greeting: "hello, world" } )
  }
} )

server.route( {
  method: "GET",
  path: "/accounts",
  handler: function( request, reply ) {
    Models.accounts.findAll().then( reply )
  }
} )

server.route( {
  method: "POST",
  path: "/accounts",
  handler: function( request, reply ) {
    Models.accounts.create( request.payload ).then( reply )
  }
} )

Models.initialize( function( results ) {
  server.start( function( err ) {
    if ( err ) throw err
    console.log( "server is running", server.info.uri )
  } )
} )
